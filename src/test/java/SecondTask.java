import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SecondTask {

    public static void main(String[] args) {
        String exePath = "C:\\distr\\chromedriver_win32\\chromedriver.exe";
        System.setProperty("webdriver.chrome.driver", exePath);
        WebDriver driver = new ChromeDriver();
//        driver.get("https://yandex.ru/");
        driver.get("https://market.yandex.ru/catalog--mobilnye-telefony/54726/list?hid=91491&onstock=1&local-offers-first=0");
        driver.manage().window().maximize();
        WebDriverWait wait = new WebDriverWait(driver, 10);
////Маркет
//        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Маркет")));
//        element.click();
////      Электроника
//        WebElement element1 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[2]/a/span")));
//        element1.click();
////      Мобильные телефоны
//        WebElement element2 = wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Мобильные телефоны")));
//        element2.click();

//        driver.switchTo().window("javasript");
//        ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);"
//                ,element3);
        ((JavascriptExecutor)driver).executeScript("scroll(0, 600)");
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"search-prepack\"]/div/div/div[3]/div/div[2]/div[3]/fieldset/ul/li[9]/div/a"))).click();

        driver.close();
    }
}
