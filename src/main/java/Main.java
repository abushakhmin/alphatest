import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

class Main {
    public static void main(String[] args) throws IOException {
        firstTask();
    }

    private static void firstTask() throws IOException {

        BufferedReader reader = new BufferedReader(new FileReader(new File("C:\\workspace\\alphatest\\src\\main\\resources\\massive.txt")));
        StringBuilder sb = new StringBuilder();
        String line;

        while((line = reader.readLine())!= null) {
        sb.append(line);
        }

        String [] store = sb.toString().split(", ");
        int[] array = strArrayToIntArray(store);

        top(array);
        System.out.println(Arrays.toString(array));

        down(array);
        System.out.println(Arrays.toString(array));
    }

    // Перевод массива из String в int
    private static int[] strArrayToIntArray(String[] a){
        int[] b = new int[a.length];
        for (int i = 0; i < a.length; i++) {
            b[i] = Integer.parseInt(a[i]);
        }

        return b;
    }
    //  Сортировка по возрастанию
    private static void top(int[] array){

        for(int i = array.length-1 ; i > 0 ; i--) {
            for (int j = 0; j < i; j++) {
                if (array[j] > array[j + 1]) {
                    int tmp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = tmp;
                }
            }
        }
    }
    //  Сортировка по убыванию
    private static void down(int[] array){

        for(int i = array.length-1 ; i > 0 ; i--) {
            for (int j = 0; j < i; j++) {
                if (array[j] < array[j + 1]) {
                    int tmp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = tmp;
                }
            }
        }
    }
}